<?php if(validation_errors()):?>
	<div class="alert alert-dismissable alert-danger">
	  <button type="button" class="close" data-dismiss="alert">×</button>
	  <?= validation_errors() ?>
	</div>
<?php endif;?>

<form class="form-inline-block" method="post" enctype="multipart/form-data">
	<input type="hidden" name="id" value="<?= isset($preference->id) ? $preference->id : set_value("preference_id") ?>" />
		<div class="form-group">
			<div class="col-md-5">
				<label for="name" class="control-label">Preference Name: </label>
				<input type="text" name="name" class="input-large form-control" value="<?= isset($preference->name) ? $preference->name : set_value("name") ?>"/>
			</div>
			<div class="col-md-5">
				<label for="category_id" class="control-label">Select a Category: </label>
				<?php $category_id = isset($preference->category_id) ? $preference->category_id : set_value("category_id"); ?>
				<?= form_dropdown('category_id', $categories, $category_id, 'class="form-control"') ?>
				<br>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-5">
				<label for="xml_parent" class="control-label">XML Parent Tag: </label>
				<input type="text" name="xml_parent" class="input-large form-control" value="<?= isset($preference->xml_parent) ? $preference->xml_parent : set_value("xml_parent") ?>"/>
			</div>
			<div class="col-md-5">
				<label for="xml_child" class="control-label">XML Child Tag: </label>
				<input type="text" name="xml_child" class="input-large form-control" value="<?= isset($preference->xml_child) ? $preference->xml_child : set_value("xml_child") ?>"/>
				<br>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-5">
				<label for="xml_attribute" class="control-label">XML Child Attribute: </label>
				<input type="text" name="xml_attribute" class="input-large form-control" value="<?= isset($preference->xml_attribute) ? $preference->xml_attribute : set_value("xml_attribute") ?>"/>
			</div>
			<div class="col-md-5">
				<label for="location" class="control-label">XML File Location: </label>
				<input type="text" name="location" class="input-large form-control" value="<?= isset($preference->location) ? $preference->location : set_value("location") ?>"/>			<br>
			</div>
		</div>
		<div class="form_group">

			<div class="col-md-5 col-md-offset-5">
				<br><br>
				<input class="btn btn-default pull-right" type="submit" value="submit changes" />
			</div>
		</div>

</form>