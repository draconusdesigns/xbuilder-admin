<div class="row">
    <table class="table table-striped tablesorter">
        <thead class="thead">
	        <th>Preference Category</th>
	        <th>Preference Name</th>
        </thead>
        <tbody id="results-body">
        <?php foreach ($preferences as $preference): ?>
            <tr>
                <td><?= $preference->category ?></td>
	            <td><a href="<?= base_url(); ?>preferences/update/<?= $preference->id?>"><?= $preference->name ?></a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>