<?php if(validation_errors()):?>
	<div class="alert alert-dismissable alert-danger">
	  <button type="button" class="close" data-dismiss="alert">×</button>
	  <?= validation_errors() ?>
	</div>
<?php endif;?>
<div class="row">
	<div class="col-md-5">
		<h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $preference->xml_child ?></h4>
	</div>
	<div class="col-md-5">
		<h4><?= $preference->xml_attribute ?></h4>
	</div>
</div>
<form class="form-inline-block" method="post" enctype="multipart/form-data">
	<input type="hidden" name="preference_id" value="<?= isset($preference->id) ? $preference->id : set_value("preference_id") ?>" />
<?php if($preference_list): ?>
	<?php foreach ($preference_list as $list): ?>
		<div class="form-group">
			<input type="hidden" name="list_id[]" value="<?= isset($list->id) ? $list->id : set_value("list_id") ?>" />
			<div class="col-md-5">
				<input type="text" name="publication[]" class="input-large form-control" value="<?= isset($list->publication) ? $list->publication : set_value("publication") ?>"/>
			</div>
			<div class="col-md-5">
				<input type="text" name="template[]" class="input-large form-control" value="<?= isset($list->template) ? $list->template : set_value("template") ?>"/>
			</div>
			<button type="button" class="btn btn-danger"><i class="fa fa-minus"></i></button> 
			<button type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
		</div>
	<?php endforeach; ?>
<?php else: ?>
	<div class="form-group">
		<div class="col-md-5">
			<input type="text" name="publication[]" class="input-large form-control" value=""/>
		</div>
		<div class="col-md-5">
			<input type="text" name="template[]" class="input-large form-control" value=""/>
		</div>
		<button type="button" class="btn btn-danger"><i class="fa fa-minus"></i></button> 
		<button type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
	</div>
<?php endif; ?>
	<div class="col-md-3 col-md-offset-7">
		<input class="btn btn-default pull-right" type="submit" value="submit changes" />
	</div>
</form>