<div class="row">
    <table class="table table-striped tablesorter">
        <thead class="thead">
	        <th>Preference Category</th>
	        <th>Preference Name</th>
			<th>XML Parent Tag</th>
			<th>XML Child Tag</th>
			<th>XML Tag Attribute</th>
			<th>XML File Location</th>
			<td class="center"><i class="fa fa-edit"></i></td>
        </thead>
        <tbody id="results-body">
        <?php foreach ($preferences as $preference): ?>
            <tr>
                <td><?= $preference->category ?></td>
	            <td><?= $preference->name ?></td>
	            <td><?= $preference->xml_parent ?></td>
	            <td><?= $preference->xml_child ?></td>
	            <td><?= $preference->xml_attribute ?></td>
	            <td><?= $preference->location ?></td>
				<td class="center"><a href="<?= base_url()?>preferences/update_configuration/<?= $preference->id?>">Edit</td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>