<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Preferences extends MY_Controller {
	
	function __construct(){
		parent::__construct();
		
	}
	function index() {
		$data['titleTag'] = "Xbuilder Preferences";
		$data['pageHeading'] = "Xbuilder Preferences";
		$data['pageSubHeading'] = "Select the xBuilder Preference file to update";
		
		$data['preferences'] = $this->preference_model->getPreferences();

		$this->load->view('templates/header', $data);
		$this->load->view('index', $data);
		$this->load->view('templates/footer', $data);
	}
	
	function update($id) {
		$data['preference'] = $this->preference_model->getPreference($id);
		
		$data['titleTag'] = "Update Preference";
		$data['pageHeading'] = "Update {$data['preference']->name} Preferences";
		$data['pageSubHeading'] = "";
		
		if ($this->form_validation->run('preference_list_validation') == FALSE){
			
			$data['preference_list'] = $this->preference_model->getPreferenceList($id);
							
			$this->load->view('templates/header', $data);
			$this->load->view('update', $data);
			$this->load->view('templates/footer', $data);
		} else {
			//update preference list items
			$this->preference_model->updatePreferenceList($data);
			redirect('/preferences/update/'.$id, 'location');
		}
	}
	
	function configuration(){
		$data['titleTag'] = "Xbuilder Configuration";
		$data['pageHeading'] = "Xbuilder Configuration";
		$data['pageSubHeading'] = "Select the xBuilder Preference file to configure";
		
		$data['preferences'] = $this->preference_model->getPreferences();

		$this->load->view('templates/header', $data);
		$this->load->view('configuration', $data);
		$this->load->view('templates/footer', $data);
	}
	function update_configuration($id){
		$data['preference'] = $this->preference_model->getPreference($id);
		
		$data['titleTag'] = "Xbuilder File Configuration";
		$data['pageHeading'] = "Update {$data['preference']->name} Settings";
		$data['pageSubHeading'] = "XML Configuration";
		
		if ($this->form_validation->run('preference_configuration_validation') == FALSE){	
			$data['categories'] = $this->preference_model->getCategoriesDropdown(); 	
								
			$this->load->view('templates/header', $data);
			$this->load->view('update_configuration', $data);
			$this->load->view('templates/footer', $data);
		} else {
			//update preference configuration
			$this->preference_model->updatePreferenceConfiguration($data);
			redirect('/preferences/configuration', 'location');
		}
	}
}