<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
    'preference_list_validation' => array(
		array(
			'field' => 'preference_id',
			'label' => 'Preference ID',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'preference_list_id',
			'label' => 'Item ID',
			'rules' => 'trim|xss_clean'
		)
    ),
    'preference_configuration_validation' => array(
		array(
			'field' => 'id',
			'label' => 'Preference ID',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'name',
			'label' => 'Preference Name',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'xml_parent',
			'label' => 'XML Parent Tag',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'xml_child',
			'label' => 'XML Child Tag',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'xml_attribute',
			'label' => 'XML Child Attribute',
			'rules' => 'trim|xss_clean|required'
		)
    )
);
