<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

    function __construct(){
		parent::__construct();
		
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		
		$user['is_admin'] = $this->is_of_role(1);
		$user['is_manager'] = $this->is_of_role(2);
		$user['is_ae'] = $this->is_of_role(3);
		$user['is_design'] = $this->is_of_role(4);
				
		$user['is_admin_or_manager'] = $this->is_of_role(1,2);
		
		$this->load->vars($user);

        //$this->output->enable_profiler(TRUE);
    }

    function is_logged_in(){
        $this->session->userdata('user_id') || redirect('users/login', 'refresh');
    }

	function is_of_role($roles) {		
        if ( !$this->session->userdata('roles') ) {
            return false;
        }
        $user_roles = $this->session->userdata('roles');
        if ( is_array($roles) ) {
            foreach ( $roles as $role ) {
                if ( in_array($role, $user_roles) ) {
					$this->data['role'] = $roles;
                    return true;
                }
            }
        } else {
            if ( in_array($roles, $user_roles) ) {	
				$this->data['role'] = array($roles);
                return true;
            }
        }
        return false;
    }

	function json($arr) {
		$this->output->enable_profiler(false);
		echo json_encode($arr);
	}
}