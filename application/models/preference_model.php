<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Preference_model extends CI_Model {

	public function __construct() {
		
	}
	
	public function getPreferences() {
		$this->db->select('preferences.*, categories.name as category');
		$this->db->join('categories', 'categories.id = preferences.category_id');
		$this->db->order_by('categories.id', 'asc');
		$query = $this->db->get("preferences");
		return $query->result();
	}

	public function getPreference($id) {
		$this->db->select('preferences.*, categories.name as category');
		$this->db->join('categories', 'categories.id = preferences.category_id');
		$this->db->where('preferences.id', $id);
		$query = $this->db->get("preferences");
		return $query->row();
	}
	
	public function getPreferenceList($preference_id) {
		$this->db->where('preference_id', $preference_id);
		$query = $this->db->get("preference_lists");
		return $query->result();
	}
	
	function getCategoriesDropdown() {
		$query = $this->db->get('categories');		
		
		$data = array(' ' => '--Select a Category--');
				
		foreach ($query->result_array() as $row){
			$data[$row['id']] = $row['name'];
		}
		
		return $data;
	}
	
	public function updatePreferenceList($data) {
		$preference_id = $this->input->post('preference_id');
		$list_id = $this->input->post('list_id');
		$publication = $this->input->post('publication');
		$template = $this->input->post('template');
		
		$update = array();
		$insert = array();
		
		$this->db->where('preference_id', $preference_id);
		$this->db->where_not_in('id', $list_id);
		$this->db->delete('preference_lists'); 
		
		foreach ($list_id as $key => $value):
			if($list_id[$key] != ''):
				$update[] = array(
				    'id' => $list_id[$key],
				    'publication' => $publication[$key],
					'template' => $template[$key]
				);
			else:
				$insert[] = array(
					'id' => $list_id[$key],
				    'publication' => $publication[$key],
					'template' => $template[$key],
					'preference_id' => $preference_id
				);
			endif;
		endforeach;
		
		if(!empty($update)):
			$this->db->update_batch('preference_lists', $update, 'id'); 
		endif;
	
		if(!empty($insert)):
			$this->db->insert_batch('preference_lists', $insert);
		endif;
	
		$list = array_merge($update, $insert);
		$this->createXML($preference_id);		
	}
	
	function createXML($preference_id) {	
		$this->db->where('id', $preference_id);
		$preference = $this->db->get("preferences");
		
		$this->db->where('preference_id', $preference_id);
		$list = $this->db->get("preference_lists");
		
		$branch = $preference->row()->xml_parent;
		$node = $preference->row()->xml_child;
		$attribute = $preference->row()->xml_attribute;
		$location = $preference->row()->location;
		
		$XML = new SimpleXMLElement("<{$branch}></{$branch}>");
		foreach ($list->result_array() as $list):
			$XMLnode = $XML->addChild($node, $list['publication']);
			$XMLnode->addAttribute($attribute, $list['template']);
		endforeach;
		
		$dom = new DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;
		$dom->loadXML($XML->asXML());
		//echo $dom->saveXML();
		$dom->save($location.$branch.'.xml');
		
		$this->session->set_flashdata('success','Preferences Updated and Exported to XML Successfully');
		return true;
	}
	
	function updatePreferenceConfiguration($data) {
		$id = $this->input->post('id');
		
		$query = array(
			'name' => $this->input->post('name'),
			'category_id' => $this->input->post('category_id'),
			'xml_parent' => $this->input->post('xml_parent'),
			'xml_child' => $this->input->post('xml_child'),
			'xml_attribute' => $this->input->post('xml_attribute'),
			'location' => $this->input->post('location')
		);
		
		$this->db->where('id', $id);
		$this->db->update('preferences', $query);
			
		$this->createXML($id);				
	}
}