$(document).ready(function() {
	$("table.tablesorter").tablesorter();
});

$(document).ready(function() {
	$(".btn-success").slice(0, -1).hide();
	
	var preference = '<div class="form-group"><div class="col-md-5"><input type="hidden" name="list_id[]" value=""><input type="text" name="publication[]" class="input-large form-control" value=""/></div><div class="col-md-5"><input type="text" name="template[]" class="input-large form-control" value=""/></div><button type="button" class="btn btn-danger"><i class="fa fa-minus"></i></button> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i></button></div>';
		
	$('body').on('click', '.form-group .btn-success', function(event) {
		event.preventDefault();
		$(preference).insertAfter('.form-group:last');
		$(".btn-success").slice(0, -1).hide();
	});

	$('body').on('click', '.form-group .btn-danger', function(event) {
		event.preventDefault();
		$(this).parent().closest('.form-group').remove();
		$(".btn-success").slice(0, -1).hide();
		$(".btn-success:last").show();
	});
});